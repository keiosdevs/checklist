<?php namespace Doomguard\Checklist\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Checklists Backend Controller
 */
class Checklists extends Controller
{
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class,
        'Backend.Behaviors.RelationController',
    ];

    /**
     * @var string formConfig file
     */
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relations.yaml';
    /**
     * @var string listConfig file
     */
    public $listConfig = 'config_list.yaml';

    /**
     * __construct the controller
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Doomguard.Checklist', 'checklist', 'checklists');
    }
}
