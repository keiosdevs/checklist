<?php namespace Doomguard\Checklist;

use Backend;
use Doomguard\Checklist\Components\Checklist;
use System\Classes\PluginBase;

/**
 * Checklist Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Doomguard Checklist',
            'description' => 'No description provided yet...',
            'author'      => 'Doomguard',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            Checklist::class => 'dg_checklist',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'doomguard.checklist.checklists' => [
                'tab'   => 'Checklist',
                'label' => 'Checklist access'
            ],
            'doomguard.checklist.items' => [
                'tab'   => 'Checklist',
                'label' => 'Items access'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return [
            'checklist' => [
                'label'       => 'Doomguard Checklist',
                'url'         => Backend::url('doomguard/checklist/checklists'),
                'icon'        => 'icon-leaf',
                'permissions' => ['doomguard.checklist.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'checklists' => [
                        'label'       => 'Checklists',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('doomguard/checklist/checklists'),
                        'permissions' => ['doomguard.checklist.checklists'],
                    ],
                    'items' => [
                        'label'       => 'Items',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('doomguard/checklist/items'),
                        'permissions' => ['doomguard.checklist.items'],
                    ],
                ]
            ],
        ];
    }
}
