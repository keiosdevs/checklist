<?php namespace Doomguard\Checklist\Components;

use Cms\Classes\ComponentBase;
use Doomguard\Checklist\Models\Checklist as ChecklistModel;
use Doomguard\Checklist\Models\Item;
use Session;

/**
 * Checklist Component
 */
class Checklist extends ComponentBase
{
    public function componentDetails(): array
    {
        return [
            'name'        => 'Checklist Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties(): array
    {
        return [];
    }

    public function onRun(): void
    {
        $this->page['checklists'] = $this->getChecklists();
        $this->page['days'] = Session::get('days', get('days', 14));
        $this->page['members'] = Session::get('members', get('members', 1));
    }

    public function onReloadChecklist(): void
    {
        $this->page['checklists'] = $this->getChecklists();
        $this->page['days'] = post('days', 14);
        $this->page['members'] = post('members', 1);
        Session::put('days', post('days', 1));
        Session::put('members', post('members', 1));
    }

    public function onSetSessionList(): void
    {
        Session::forget('checklist');
        $post = post('item-checkbox');
        $checkedList = [];
        if ($post) {
            $checkedList = array_keys($post);
        }
        Session::put('checklist', $checkedList);

        $this->onReloadChecklist();
    }

    public function countAmount(Item $item, int $days, int $members): int
    {
        if ($item->is_per_day && $item->is_per_member) {
            return ceil($item->quantity * $days * $members);
        }
        if ($item->is_per_member) {
            return ceil($item->quantity * $members);
        }
        if ($item->is_per_day) {
            return ceil($item->quantity * $days);
        }

        return ceil($item->quantity);
    }

    public function isSetInSession(int $itemId): bool
    {

        $list = Session::get('checklist', []);

        return in_array($itemId, $list);
    }

    public function getPriorityLabel(int $priority): string
    {
        switch ($priority) {
            case 1:
                return 'Crucial';
            case 2:
                return 'Very Important';
            case 3:
                return 'Important';
            case 4:
                return 'Very Useful';
            default:
                return 'Useful';
        }
    }

    private function getChecklists()
    {
        return ChecklistModel::where('is_visible', true)->with(['items' => function ($q) {
            $q->orderBy('priority', 'asc')->orderBy('name', 'asc');
        }])->get();
    }
}
