<?php namespace Doomguard\Checklist\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateItemsTable Migration
 */
class CreateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('doomguard_checklist_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->boolean('is_per_member')->default(false);
            $table->boolean('is_per_day')->default(false);
            $table->integer('quantity')->default(1);
            $table->integer('priority')->default(1);
            $table->string('unit')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('doomguard_checklist_items');
    }
}
