<?php namespace Doomguard\Checklist\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateChecklistsTable Migration
 */
class CreateChecklistsTable extends Migration
{
    public function up()
    {
        Schema::create('doomguard_checklist_checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_visible');
            $table->timestamps();
        });
        Schema::create('doomguard_checklist_checklists_items', function (Blueprint $table) {
            $table->integer('checklist_id')->index();
            $table->integer('item_id')->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('doomguard_checklist_checklists');
        Schema::dropIfExists('doomguard_checklist_checklists_items');
    }
}
